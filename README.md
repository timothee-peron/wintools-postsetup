# Desktop initialisation on WIN platform

This procedure follows WIN10 install to have a usable system.
Sometimes constructors ship bloatware, remove it first.
Sometimes the installer defaults settings need to be changed, they are listed after every link.
This list includes non-free (libre) software.

## Windows Settings

Power usage: full performance when charging

User profile picture
WallPaper
Custom sounds

Windows explorer:
- open %userprofile% and pin to favorites
- Change folder open by default to "my computer"
- Disable History
- Open by double click

## General Tools

- [Firefox](https://www.mozilla.org/firefox/download/)
  - Multi-Account Containers
  - uBlock Origin
- [Thunderbird](https://www.thunderbird.net/)
- [Google Chrome](https://www.google.com/chrome/)
- [7zip](https://7-zip.org/download.html)
  - after setup, open .zip .7z .rar .tar .tar.gz with ./7zFM.exe
- [KeepassXC](https://keepassxc.org/download/) Password Manager
  - Disable windows credentials integration
- [Notepad++](https://notepad-plus-plus.org/downloads/)
- [pdf24](https://tools.pdf24.org) Free PDF tools
  - setup: install for all users
  - set output folder to %userprofile%\\Documents (for all users E:\\Temp)
  - disable tray icon
  - disable context menu, shell extension, browser extension (open as Admin for all users)
  - disable online features
- [sumatra PDF](https://www.sumatrapdfreader.org/download-free-pdf-viewer) A Libre pdf viewer
- [greenshot](https://getgreenshot.org/downloads/) Libre screenshot taker
  - setup: disable imgur integration
  - setup: disable windows startup
  - Set save output to %userprofile%\\Pictures\\Screenshots
  - Improve jpeg quality to 90%

## Graphics tools

- [Gimp](https://www.gimp.org/downloads/) general image editor
- [Krita](https://krita.org/) image editor, good at drawing
- [Inkscape](https://inkscape.org) SVG editor
  - setup: disable python and extensions
  - setup: disable translations 
- [Paint.Net](https://www.getpaint.net/download.html) lightweight image editor
  - setup: disable auto updates
- [Libresprite](https://libresprite.github.io/) pixel art editor, libre fork of aseprite 
- [Blender](https://www.blender.org/) 3D editor

## Development Tools

- [vim](https://www.vim.org/)
- [neovim](https://neovim.io/)
  - powershell: winget install Neovim.Neovim
- [Frhed hex editor](https://frhed.sourceforge.net/)
- [putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
- [filezilla](https://filezilla-project.org/)
  - setup: don't install sponsored bloatware
  - setup: unselect shell extension
- [WinSCP](https://winscp.net/)
  - setup: unselect all additional components (included in putty and 50+MB translations)
  - setup: disable auto updates, explorer integration, url support
- [git](https://git-scm.com/download/win)
- [VSCodium](https://vscodium.com/)
- [Rapid Environment Editor](https://www.rapidee.com/en/download)
- [winmerge](https://winmerge.org/downloads/)
  - setup: disable all optional components
  - setup: disable windows integration
- [Fira Code font](https://github.com/tonsky/FiraCode/releases)

## Sysinternal suite

Download at [microsoft.com](https://learn.microsoft.com/en-us/sysinternals/downloads/)
Extract contents to %ProgramFiles%\SysinternalsSuite
Most programs have a 64 bits version : *.exe > *64.exe
You can use flags -nobanner -accepteula to hide banner and prevent displaying eula popup.

Some interesting tools:
- Autoruns display and edit auto-start programs autorun (autorun)
- CPUSTRES create threads with various load and priority
- procexp64 is a process explorer, more advanced than task manager
- RDCMan remote desktops manager
- ShareEnum information about shared folders
- tcpview shows all tcp and udp endpoints
- VMMap analyses one program's memory usage

## Benchmark tools

- GPU: [Unigine](https://unigine.com)
